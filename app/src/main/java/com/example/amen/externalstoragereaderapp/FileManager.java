package com.example.amen.externalstoragereaderapp;

import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by amen on 1/18/17.
 */

public enum FileManager {
    INSTANCE;

    private static final String folder_name = "accelerometer";

    public String readFile(String fileName)  {
        // TODO: czyta plik i zwraca jego wartość

        String line;
        StringBuilder sb = new StringBuilder();
//        try {
//            FileReader fileReader = new FileReader(fileName);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//
//            line = bufferedReader.readLine();
//
//            while (line != null) {
//                line = bufferedReader.readLine();
//
//                 sb.append(line + "\n");
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

                try (FileReader fileReader = new FileReader(fileName);
                     Scanner scanner = new Scanner(fileReader);) {

            while (scanner.hasNext()) {
                line = scanner.nextLine();

                 sb.append(line + "\n");

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public File[] getFilesFromFolder(){

        File folderOrFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + folder_name);
        File[] fileList = null;
        if (!folderOrFile.exists() || folderOrFile.isDirectory()) {
            folderOrFile.mkdir();
            fileList = folderOrFile.listFiles();
        }

        return fileList;
    }
}
